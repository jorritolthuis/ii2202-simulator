|--------------------|
| Running Manchester |
|--------------------|
SEED = 1234
N_ITERATIONS = 10000000
BER = 0.005000

The simulation took 1.825944 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.002627% of bits were received incorrectly
0.995066% of bits were detected to be incorrect

The longest incorrect burst was 4 bits
49998622, 50001378, 1340, 1287, 2627
0, 4, 0, 995066
|--------------------|
| Running Manchester |
|--------------------|
SEED = 1235
N_ITERATIONS = 10000000
BER = 0.005000

The simulation took 1.788611 seconds

100000000 bits were generated, which is equal to 12500.0 KB
49.99% of the generated signal were 1s
0.002474% of bits were received incorrectly
0.995708% of bits were detected to be incorrect

The longest incorrect burst was 4 bits
49989603, 50010397, 1245, 1229, 2474
0, 4, 0, 995708
|--------------------|
| Running Manchester |
|--------------------|
SEED = 1236
N_ITERATIONS = 10000000
BER = 0.005000

The simulation took 1.799097 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.002625% of bits were received incorrectly
0.994477% of bits were detected to be incorrect

The longest incorrect burst was 4 bits
50002266, 49997734, 1299, 1326, 2625
0, 4, 0, 994477
|--------------------|
| Running Manchester |
|--------------------|
SEED = 1237
N_ITERATIONS = 10000000
BER = 0.005000

The simulation took 1.797833 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.002412% of bits were received incorrectly
0.996037% of bits were detected to be incorrect

The longest incorrect burst was 4 bits
49999347, 50000653, 1277, 1135, 2412
0, 4, 0, 996037
