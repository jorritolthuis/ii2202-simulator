|--------------|
| Running NRZI |
|--------------|
SEED = 1234
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 1.788845 seconds

100000000 bits were generated, which is equal to 12500.0 KB
49.99% of the generated signal were 1s
50.451770% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 25 bits
A burst was ongoing, with current length 1
49992467, 50007533, 22973543, 27478227, 50451770
1, 25, 1, 0
|--------------|
| Running NRZI |
|--------------|
SEED = 1235
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 1.793138 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
50.445625% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 26 bits
49996285, 50003715, 22976056, 27469569, 50445625
0, 26, 0, 0
|--------------|
| Running NRZI |
|--------------|
SEED = 1236
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 1.782240 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
50.444771% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 32 bits
49999591, 50000409, 22977966, 27466805, 50444771
0, 32, 0, 0
|--------------|
| Running NRZI |
|--------------|
SEED = 1237
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 1.811956 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
50.448304% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 25 bits
A burst was ongoing, with current length 2
50000562, 49999438, 22980260, 27468044, 50448304
2, 25, 1, 0
