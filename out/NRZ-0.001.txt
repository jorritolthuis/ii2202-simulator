|-------------|
| Running NRZ |
|-------------|
SEED = 1234
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 0.935862 seconds

100000000 bits were generated, which is equal to 12500.0 KB
49.99% of the generated signal were 1s
0.100096% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
49992467, 50007533, 50152, 49944
0, 2, 0, 0
|-------------|
| Running NRZ |
|-------------|
SEED = 1235
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 0.903495 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.099782% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
49996285, 50003715, 50120, 49662
0, 2, 0, 0
|-------------|
| Running NRZ |
|-------------|
SEED = 1236
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 0.904268 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.100372% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
49999591, 50000409, 50246, 50126
0, 2, 0, 0
|-------------|
| Running NRZ |
|-------------|
SEED = 1237
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 0.900912 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.099901% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
50000562, 49999438, 49909, 49992
0, 2, 0, 0
