|---------------------------------|
| Running Differential Manchester |
|---------------------------------|
SEED = 1234
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 1.804725 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.199886% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
49998622, 50001378, 99750, 100136, 199886
0, 2, 0, 0
|---------------------------------|
| Running Differential Manchester |
|---------------------------------|
SEED = 1235
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 1.806083 seconds

100000000 bits were generated, which is equal to 12500.0 KB
49.99% of the generated signal were 1s
0.199653% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
49989603, 50010397, 100033, 99620, 199653
0, 2, 0, 0
|---------------------------------|
| Running Differential Manchester |
|---------------------------------|
SEED = 1236
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 1.801624 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.199726% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 2 bits
50002266, 49997734, 100053, 99673, 199726
0, 2, 0, 0
|---------------------------------|
| Running Differential Manchester |
|---------------------------------|
SEED = 1237
N_ITERATIONS = 10000000
BER = 0.001000

The simulation took 1.808871 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
0.199269% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 3 bits
49999347, 50000653, 100027, 99242, 199269
0, 3, 0, 0
