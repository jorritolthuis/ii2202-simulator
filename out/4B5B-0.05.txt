|--------------|
| Running 4B5B |
|--------------|
SEED = 1234
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 5.623059 seconds

400000000 bits were generated, which is equal to 50000.0 KB
50.00% of the generated signal were 1s
3.866702% of bits were received incorrectly
15.258956% of bits were detected to be incorrect

The longest incorrect burst was 36 bits
200007796, 199992204, 6479534, 8987272, 15466806
0, 36, 0, 61035824
|--------------|
| Running 4B5B |
|--------------|
SEED = 1235
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 5.589176 seconds

400000000 bits were generated, which is equal to 50000.0 KB
50.00% of the generated signal were 1s
3.865381% of bits were received incorrectly
15.255885% of bits were detected to be incorrect

The longest incorrect burst was 40 bits
200010818, 199989182, 6476664, 8984859, 15461523
0, 40, 0, 61023540
|--------------|
| Running 4B5B |
|--------------|
SEED = 1236
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 5.628041 seconds

400000000 bits were generated, which is equal to 50000.0 KB
50.00% of the generated signal were 1s
3.865874% of bits were received incorrectly
15.259582% of bits were detected to be incorrect

The longest incorrect burst was 40 bits
200001886, 199998114, 6479073, 8984422, 15463495
0, 40, 0, 61038328
|--------------|
| Running 4B5B |
|--------------|
SEED = 1237
N_ITERATIONS = 10000000
BER = 0.050000

The simulation took 5.576185 seconds

400000000 bits were generated, which is equal to 50000.0 KB
50.00% of the generated signal were 1s
3.867192% of bits were received incorrectly
15.256804% of bits were detected to be incorrect

The longest incorrect burst was 44 bits
199997859, 200002141, 6480665, 8988104, 15468769
0, 44, 0, 61027216
