|--------------|
| Running NRZI |
|--------------|
SEED = 1234
N_ITERATIONS = 10000000
BER = 0.000100

The simulation took 1.654116 seconds

100000000 bits were generated, which is equal to 12500.0 KB
49.99% of the generated signal were 1s
50.002733% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 25 bits
A burst was ongoing, with current length 1
49992467, 50007533, 22501001, 27501732, 50002733
1, 25, 1, 0
|--------------|
| Running NRZI |
|--------------|
SEED = 1235
N_ITERATIONS = 10000000
BER = 0.000100

The simulation took 1.672676 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
49.999980% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 25 bits
49996285, 50003715, 22504759, 27495221, 49999980
0, 25, 0, 0
|--------------|
| Running NRZI |
|--------------|
SEED = 1236
N_ITERATIONS = 10000000
BER = 0.000100

The simulation took 1.679205 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
49.994956% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 24 bits
49999591, 50000409, 22506191, 27488765, 49994956
0, 24, 0, 0
|--------------|
| Running NRZI |
|--------------|
SEED = 1237
N_ITERATIONS = 10000000
BER = 0.000500

The simulation took 1.651733 seconds

100000000 bits were generated, which is equal to 12500.0 KB
50.00% of the generated signal were 1s
50.005700% of bits were received incorrectly
0.000000% of bits were detected to be incorrect

The longest incorrect burst was 25 bits
A burst was ongoing, with current length 2
50000562, 49999438, 22510203, 27495497, 50005700
2, 25, 1, 0
