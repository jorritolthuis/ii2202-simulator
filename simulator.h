/*
 * Author: Jorrit Olthuis
 * Date: September 26, 2019
 */

// Random seed to generate input
//#define SEED            (1234)

// Number of iterations, each iteration has 10 input bits
// Only 4B5B has 40 input bits per iteration
#define N_ITERATIONS    (10000000)

// 1/how often one bit flips on the channel
#define BIT_ERROR_RATE  (0.0001)
