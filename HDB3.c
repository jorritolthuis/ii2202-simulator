/*
 * Author: Jorrit Olthuis
 * Date: October 4, 2019
 */

#include <string.h>
#include "HDB3.h"

void encode(unsigned char input[10], unsigned char output[10])
{
    memcpy(output, input, 10);
}

void decode(unsigned char input[10], unsigned char output[10])
{
    memcpy(output, input, 10);
}
