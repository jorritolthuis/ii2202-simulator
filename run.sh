#! /bin/bash

# argument 1 is encoding name from makefile
# argument 2 is setting name

make clean

if [ "$#" -ne 2 ]; then
    echo "ERROR incorrect number of arguements"
    exit 1
fi

OUTPUT=out/$1-$2.txt

make $1 DEFS=-DSEED=1234
echo "Simulation 1 started..."
./simulator >> $OUTPUT
echo "done."

make $1 DEFS=-DSEED=1235
echo "Simulation 2 started..."
./simulator >> $OUTPUT
echo "done."

make $1 DEFS=-DSEED=1236
echo "Simulation 3 started..."
./simulator >> $OUTPUT
echo "done."

make $1 DEFS=-DSEED=1237
echo "Simulation 4 started..."
./simulator >> $OUTPUT
echo "done."
