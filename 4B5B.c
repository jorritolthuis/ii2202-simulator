/*
 * Author: Jorrit Olthuis
 * Date: October 4, 2019
 */

#include "4B5B.h"

unsigned char enc_table[16] = {0x1E, 0x09, 0x14, 0x15, 0x0A, 0x0B, 0x0E, 0x0F,
                               0x12, 0x13, 0x16, 0x17, 0x1A, 0x1B, 0x1C, 0x1D};

#define EMPT 0xFF

unsigned char dec_table[32] = {EMPT, EMPT, EMPT, EMPT, EMPT, EMPT, EMPT, EMPT,
                               EMPT, 0x01, 0x04, 0x05, EMPT, EMPT, 0x06, 0x07,
                               EMPT, EMPT, 0x08, 0x09, 0x02, 0x03, 0x0A, 0x0B,
                               EMPT, EMPT, 0x0C, 0x0D, 0x0E, 0x0F, EMPT, EMPT};

void encode(unsigned char input[40], unsigned char output[50])
{
    int i;
    for(i=0; i<10; ++i) {
        unsigned int in_val = (input[4*i] << 3) |
                                    (input[4*i+1] << 2) |
                                    (input[4*i+2] << 1) |
                                    (input[4*i+3]);
        unsigned char out_val = enc_table[in_val];
        output[5*i+4] = out_val & 0x1;
        output[5*i+3] = (out_val >> 1) & 0x1;
        output[5*i+2] = (out_val >> 2) & 0x1;
        output[5*i+1] = (out_val >> 3) & 0x1;
        output[5*i] = (out_val >> 4) & 0x1;
    }
}

void decode(unsigned char input[50], unsigned char output[40])
{
    int i;
    for (i=0; i<10; ++i) {
        unsigned int in_val = (input[5*i] << 4) |
                                    (input[5*i+1] << 3) |
                                    (input[5*i+2] << 2) |
                                    (input[5*i+3] << 1) |
                                    (input[5*i+4]);
        unsigned char out_val = dec_table[in_val];
        
        if (out_val == EMPT) {
            // At least one bit is guaranteed to be wrong.
            output[4*i] = 2;
            output[4*i+1] = 2;
            output[4*i+2] = 2;
            output[4*i+3] = 2;
        } else {
            // Decode, we can't detect an error so assume it's fine.
            output[4*i] = (out_val >> 3) & 0x1;
            output[4*i+1] = (out_val >> 2) & 0x1;
            output[4*i+2] = (out_val >> 1) & 0x1;
            output[4*i+3] = out_val & 0x1;
        }
    }
}
