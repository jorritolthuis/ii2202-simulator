/*
 * Author: Jorrit Olthuis
 * Date: October 4, 2019
 */

#include "NRZI.h"

/* This implements NRZI (NRZS, Non-return-to-zero space)
 For our analysis this is equivalent to NRZM */

unsigned char prev_char_enc = 0;
unsigned char prev_char_dec = 0;

void encode(unsigned char input[10], unsigned char output[10])
{
    int i;
    for (i=0; i<10; ++i) {
        if (input[i] == 0) {
            output[i] = prev_char_enc;
        } else {
            prev_char_enc = (prev_char_enc + 1) & 0x1; // Flip bit
            output[i] = prev_char_enc;
        }
    }
}

void decode(unsigned char input[10], unsigned char output[10])
{
    int i;
    for (i=0; i<10 ; ++i) {
        if (input[i] == prev_char_dec) {
            output[i] = 0;
        } else {
            prev_char_dec = (prev_char_enc + 1) & 0x1; // Flip bit
            output[i] = 1;
        }
    }
}
