/*
 * Author: Jorrit Olthuis
 * Date: October 4, 2019
 */

void encode(unsigned char input[10], unsigned char output[10]);
void decode(unsigned char input[10], unsigned char output[10]);
