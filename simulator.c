/*
 * Author: Jorrit Olthuis
 * Date: September 26, 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "simulator.h"

#ifdef NRZ
#include "NRZ.h"
#endif
#ifdef MANCHESTER
#include "manchester.h"
#endif
#ifdef DIFF_MANCHESTER
#include "diff-manchester.h"
#endif
#ifdef NRZI
#include "NRZI.h"
#endif
#ifdef _4B5B
#include "4B5B.h"
#endif
#ifdef HDB3
#include "HDB3.h"
#endif

/* Statistics */
int ones, zeros = 0;
int zeros_received_as_one, ones_received_as_zero = 0;
int detected_errors = 0;
clock_t start_time = 0.0;
// Burst is a sequence of incorrect bits
int current_burst_length, longest_burst = 0;
char burst_is_ongoing = 0;

void channel(unsigned char channel_bits[10]);
void print_statistics();

int main()
{
    start_time = clock();
    
    int it; // Simulation iterator
    int i; // Loop iterator
    
    int r; // Random input
    
#if defined(_4B5B)
    unsigned char input[40];
    unsigned char channel_bits[50]; // Encoded bits (5x)
    unsigned char output[40]; // Decoded signal
#elif defined(MANCHESTER) || defined(DIFF_MANCHESTER)
    unsigned char input[10]; // Random input converted into bits
    unsigned char channel_bits[20]; // Encoded signal, at double frequency
    unsigned char output[10]; // Decoded signal
#else
    unsigned char input[10]; // Random input converted into bits
    unsigned char channel_bits[10]; // Encoded signal
    unsigned char output[10]; // Decoded signal
#endif
    
    // Set random seed
    srand(SEED);
    
    for (it=0; it<N_ITERATIONS; ++it) {
#if defined(_4B5B)
        for (i=0; i<40; ++i) {
            // Create random number per 10 inputs
            if (i % 10 == 0) r = rand();
            input[i] = (r >> (i % 10)) & 0x1;
        }
#else
        r = rand(); // A number between 0 and at least 32767 (= minimum value of RAND_MAX)
        // Create 10 bits from that
        for (i=0; i<10; ++i) {
            input[i] = (r>>i) & 0x1;
        }
#endif
        encode(input, channel_bits);
        
        channel(channel_bits);
#if defined(MANCHESTER) || defined(DIFF_MANCHESTER)
        channel(&channel_bits[10]); // Double frequency
#elif defined(_4B5B)
        // Five time frequency
        channel(&channel_bits[10]);
        channel(&channel_bits[20]);
        channel(&channel_bits[30]);
        channel(&channel_bits[40]);
#endif
        
        decode(channel_bits, output);
        
        // Write results
#if defined(_4B5B)
        for (i=0; i<40; ++i) {
#else
        for (i=0; i<10; ++i) {
#endif
            if(input[i] == 1) {
                ones++;
                if(output[i] == 0) ones_received_as_zero++;
                if(output[i] == 2) detected_errors++;
            } else {
                zeros++;
                if(output[i] == 1) zeros_received_as_one++;
                if(output[i] == 2) detected_errors++;
            }

            current_burst_length = input[i] != output[i] ? current_burst_length+1 : 0;
            burst_is_ongoing = input[i] != output[i] ? 1 : 0;
            longest_burst = current_burst_length > longest_burst ? current_burst_length : longest_burst;
        }
    }
    print_statistics();
    
    return 0;
}


void channel(unsigned char channel_bits[10])
{
    int i;
    for (i=0; i<10; ++i) {
        int r = rand() % (int)(1/BIT_ERROR_RATE);
        if (r == 0) channel_bits[i] = (channel_bits[i] + 1) & 0x1; // Flip bit
    }
}

void print_statistics()
{
#ifdef NRZ
    printf("|-------------|\n| Running NRZ |\n|-------------|\n");
#endif
#ifdef MANCHESTER
    printf("|--------------------|\n| Running Manchester |\n|--------------------|\n");
#endif
#ifdef DIFF_MANCHESTER
    printf("|---------------------------------|\n| Running Differential Manchester |\n|---------------------------------|\n");
#endif
#ifdef NRZI
    printf("|--------------|\n| Running NRZI |\n|--------------|\n");
#endif
#ifdef _4B5B
    printf("|--------------|\n| Running 4B5B |\n|--------------|\n");
#endif
#ifdef HDB3
    printf("|--------------|\n| Running HDB3 |\n|--------------|\n");
#endif
    printf("SEED = %d\n", SEED);
    printf("N_ITERATIONS = %d\n", N_ITERATIONS);
    printf("BER = %f\n\n", BIT_ERROR_RATE);
    
    // Print results
    printf("The simulation took %f seconds\n\n", (double) (clock()-start_time)/CLOCKS_PER_SEC);
    
    printf("%d bits were generated, which is equal to %.1f KB\n", ones+zeros
           , (double)(ones+zeros)/8000);
    printf("%.2f%% of the generated signal were 1s\n", (double) ones / (ones+zeros) * 100);
    printf("%.6f%% of bits were received incorrectly\n", (double)(zeros_received_as_one+ones_received_as_zero)/(ones+zeros) * 100);
    printf("%.6f%% of bits were detected to be incorrect\n\n", (double)detected_errors/(ones+zeros) * 100);

    printf("The longest incorrect burst was %d bits\n", longest_burst);
    if(burst_is_ongoing) {
        printf("A burst was ongoing, with current length %d\n", current_burst_length);
    }
    
    printf("%d, %d, %d, %d, %d\n%d, %d, %d, %d\n",
           ones, zeros, ones_received_as_zero, zeros_received_as_one, ones_received_as_zero + zeros_received_as_one,
           current_burst_length, longest_burst, burst_is_ongoing, detected_errors);
}
