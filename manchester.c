/*
 * Author: Jorrit Olthuis
 * Date: September 28, 2019
 */

#include <string.h>
#include "manchester.h"

// Manchester according to IEEE 802.3

void encode(unsigned char input[10], unsigned char output[20])
{
    int i;
    for(i=0; i<10; ++i) {
        if(input[i] == 1) {
            output[i*2]=0;
            output[i*2+1]=1;
        } else {
            output[i*2]=1;
            output[i*2+1]=0;
        }
    }
}

void decode(unsigned char input[20], unsigned char output[10])
{
    int i;
    for (i=0; i<10; ++i) {
        if(input[2*i] && !input[2*i+1]) {
            // 0
            output[i] = 0;
        } else if(!input[2*i] && input[2*i+1]) {
            // 1
            output[i] = 1;
        } else {
            // Error, make a guess
            output[i] = 2;
        }
    }
}
