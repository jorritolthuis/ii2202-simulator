/*
 * Author: Jorrit Olthuis
 * Date: September 26, 2019
 */

#include <string.h>
#include "NRZ.h"

void encode(unsigned char input[10], unsigned char output[10])
{
    memcpy(output, input, 10);
}

void decode(unsigned char input[10], unsigned char output[10])
{
    memcpy(output, input, 10);
}
