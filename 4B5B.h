/*
 * Author: Jorrit Olthuis
 * Date: October 4, 2019
 */

void encode(unsigned char input[40], unsigned char output[50]);
void decode(unsigned char input[50], unsigned char output[40]);
