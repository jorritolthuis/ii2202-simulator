/*
 * Author: Jorrit Olthuis
 * Date: September 28, 2019
 */

void encode(unsigned char input[10], unsigned char output[20]);
void decode(unsigned char input[20], unsigned char output[10]);
