/*
 * Author: Jorrit Olthuis
 * Date: October 4, 2019
 */

#include <string.h>
#include "diff-manchester.h"

// Differential Manchester according to IEEE 802.5
unsigned char last_enc_signal = 0;
unsigned char last_dec_signal = 0;

void encode(unsigned char input[10], unsigned char output[20])
{
    int i;
    for(i=0; i<10; ++i) {
        if(input[i] == 1) {
            // Flip last siignal
            last_enc_signal = (last_enc_signal + 1) & 0x1;
            output[i*2] = last_enc_signal;
            // Don't flip halfway
            output[i*2+1] = last_enc_signal;
        } else {
            // Flip last signal
            last_enc_signal = (last_enc_signal + 1) & 0x1;
            output[i*2] = last_enc_signal;
            // Flip it back
            last_enc_signal = (last_enc_signal + 1) & 0x1;
            output[i*2+1] = last_enc_signal;
        }
    }
}

void decode(unsigned char input[20], unsigned char output[10])
{
    /* There is some form of error detection possible, by verifying transitions between bits.
     However, this only allows to narrow it down to two bits (for a single bit flip).
     This code does not take this into account. */
    
    int i;
    for (i=0; i<10; ++i) {
        if(input[2*i] == input[2*i+1]) {
            // 0
            output[i] = 1;
        } else {
            // 1
            output[i] = 0;
        }
    }
}
