CC=gcc
CFLAGS?=

# simulator needs to be phony to force rebuild, same for simulator.o
# Especially important when changing encodings
.PHONY: diff-manchester NRZI 4B5B HDB3 NRZ manchester clean simulator simulator.o

simulator.o: simulator.c simulator.h
	$(CC) -O3 -o $@ -c $< $(CFLAGS) $(DEFS)

%.o: %.c %.h
	$(CC) -O3 -o $@ -c $< $(CFLAGS) $(DEFS)

simulator: simulator.o
	$(CC) -o simulator $^ $(OBJECTS) $(DEFS)
	
#########################################################

manchester: CFLAGS += -DMANCHESTER
manchester: OBJECTS=manchester.o
manchester: manchester.o simulator

NRZ: CFLAGS += -DNRZ
NRZ: OBJECTS=NRZ.o
NRZ: NRZ.o simulator

diff-manchester: CFLAGS += -DDIFF_MANCHESTER
diff-manchester: OBJECTS=diff-manchester.o
diff-manchester: diff-manchester.o simulator

NRZI: CFLAGS += -DNRZI
NRZI: OBJECTS=NRZI.o
NRZI: NRZI.o simulator

4B5B: CFLAGS += -D_4B5B
4B5B: OBJECTS=4B5B.o
4B5B: 4B5B.o simulator

HDB3: CFLAGS += -DHDB3
HDB3: OBJECTS=HDB3.o
HDB3: HDB3.o simulator

clean:
	rm *.o
	rm simulator
